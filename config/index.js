var metalsmith = require("metalsmith");
var watch = require("metalsmith-watch");
var markdown = require("metalsmith-markdown");
var layouts = require("metalsmith-layouts");
var permalinks = require("metalsmith-permalinks");

var bs = require("browser-sync").create();

metalsmith(__dirname)
    .metadata({
        title: "My Static Site & Blog",
        description: "It's about saying »Hello« to the World.",
        generator: "Metalsmith",
        url: "http://www.metalsmith.io/"
    })
    .source("../pages")
    .destination("../build")
    .clean(true)
    .use(markdown())
    .use(permalinks())
    .use(layouts({
        engine: "handlebars",
        directory: "../layouts"
    }))
    // hot reload
    .use(watch({
        paths: {
            '../layouts/**/*': '**/*',
            '../pages/**/*': '**/*'
        }
    }))
    .build(function (err, files) {
        bs.init({
            server: "build",
            notify: false,
            files: [
                "layouts/**/*",
                "pages/**/*"
            ]
        })
        if (err)
        {
            console.log(err)
        }
    });
